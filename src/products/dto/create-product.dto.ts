import { IsNotEmpty, IsPositive, Length } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(10, 40)
  name: string;

  @IsNotEmpty()
  @IsPositive()
  price: number;
}
