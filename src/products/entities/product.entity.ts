import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn({ name: 'product_id' })
  id: number;

  @Column({ name: 'product_name', length: 40, unique: true })
  name: string;

  @Column({ name: 'product_price' })
  price: number;
}
